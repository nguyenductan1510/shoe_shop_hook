import Shoe_Hook from "./Shoe_Hook/Shoe_Hook";

import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Header from "./Header/Header";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<Shoe_Hook />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
