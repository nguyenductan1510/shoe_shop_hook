import React from "react";

export default function ItemHook({ data, hendLeOnclick }) {
  return (
    <div className=" col-3 p-4">
      <div className="card ">
        <img src={data.image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{data.name}</h5>
          <h5>{data.price}$</h5>
          {/* <p className="card-text">{description}</p> */}
          {/* <p className="card-text">{shortDescription}</p> */}
          <a
            href="#"
            onClick={() => {
              hendLeOnclick(data);
            }}
            className="btn btn-primary"
          >
            Add to carts
          </a>
        </div>
      </div>
    </div>
  );
}
