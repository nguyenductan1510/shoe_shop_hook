import Cart from "./Cart";
import { dataShoe } from "./Data_Shoe";
import React, { useEffect, useState } from "react";
import ItemHook from "./ItemHook";

export default function Shoe_Hook() {
  const [list, setList] = useState([]);
  const [cart, setCart] = useState([]);
  let data = dataShoe;
  useEffect(() => {
    return setList(data);
  }, []);

  let hendLeAddToCart = (shoe) => {
    let cloneCart = [...cart];

    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });

    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }

    setCart(cloneCart);
  };
  let renderDataList = () => {
    return list.map((item) => {
      return <ItemHook data={item} hendLeOnclick={hendLeAddToCart} />;
    });
  };
  return (
    <div className="container">
      <Cart cart={cart} />
      <div
        className="row
      "
      >
        {renderDataList()}
      </div>
    </div>
  );
}
